package com.kolab.fatihsenturk.hurriyethackhaton;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class MainActivity extends ActionBarActivity {

    private EditText minFiyat;
    private EditText maxFiyat;
    private EditText il;
    private EditText ilce;
    private EditText odaSayisi;
    private EditText metreKare;
    private Spinner spinner;
    private Drawer resultAppended = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseUser parseUser = new ParseUser();
        parseUser.setEmail("fatih@fatih.com");
        parseUser.setUsername("fatih");
        parseUser.setPassword("123456");
        parseUser.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(MainActivity.this, "burasi merkez singup oldu", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        AccountHeader accountHeaderForUser = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(new ProfileDrawerItem().
                        withName("Ad Soyad").
                        withEmail("ad@soyad.com").
                        withEnabled(true))
                .build();

        resultAppended = new DrawerBuilder().
                withAccountHeader(accountHeaderForUser)
                .withActivity(this)
                .withSavedInstance(savedInstanceState)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Anasayfa").withIcon(R.drawable.pushreal).withIdentifier(54),
                        new PrimaryDrawerItem().withName("Detayli Arama").withIcon(R.drawable.pushreal).withIdentifier(123),
                        new PrimaryDrawerItem().withName("Ilan Ver ").withIcon(R.drawable.pushreal).withIdentifier(33),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName("Ilanlaarim").withIcon(R.drawable.pushreal).withIdentifier(22),
                        new SecondaryDrawerItem().withName("Uyelik İslemlerim").withIcon(R.drawable.pushreal).withIdentifier(5),
                        new SecondaryDrawerItem().withName("Favorilerim").withIcon(R.drawable.pushreal).withIdentifier(4),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName("Kayitli Aramalarim").withIcon(R.drawable.pushreal).withIdentifier(3),
                        new SecondaryDrawerItem().withName("Mesajlarim").withIcon(R.drawable.mesaj1).withIdentifier(2),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName("Bildirim Kur").withIcon(R.drawable.saat1).withIdentifier(1),
                        new DividerDrawerItem()
                ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        if (drawerItem.getIdentifier() == 1) {
                            Intent intent = new Intent(MainActivity.this, Veriler.class);
                            startActivity(intent);
                        }
                        return false;
                    }
                }).build();
    }
}

package com.kolab.fatihsenturk.hurriyethackhaton;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class Veriler extends AppCompatActivity {

    private EditText il;
    private EditText ilce;
    private EditText maxFiyat;
    private EditText minFiyat;
    private EditText kaySayi;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veriler);

        il = (EditText)findViewById(R.id.il);
        ilce= (EditText)findViewById(R.id.ilce);
        maxFiyat = (EditText)findViewById(R.id.MaxFiyat);
        minFiyat = (EditText)findViewById(R.id.minFiyat);
        kaySayi = (EditText)findViewById(R.id.katsayisi);
        btn = (Button)findViewById(R.id.buton);

        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = ProgressDialog.show(Veriler.this,"Lutfen Bekleyin","Veriler yukleniyor",true);
                progressDialog.setCancelable(true);
                ParseObject parseObject = new ParseObject("TestObject");

                parseObject.put("il",il.getText().toString());
                parseObject.put("ilce",ilce.getText().toString());

                parseObject.put("maxFiyat",Integer.parseInt(maxFiyat.getText().toString()));
                parseObject.put("minFiyat",Integer.parseInt(minFiyat.getText().toString()));
                parseObject.put("kacinciKat",Integer.parseInt(kaySayi.getText().toString()));

                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        progressDialog.dismiss();
                        if (e==null){
                            Toast.makeText(Veriler.this,"Basarili bir sekilde kayit alindi",Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(Veriler.this,e.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

    }

}
